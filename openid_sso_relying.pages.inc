<?php

/**
 * @file
 * Login/Logout/Registration/Password pages for the OpenID SSO Relying module.
 */

/**
 * Page callback for initiating an authentication request. Logs out user before
 * initiation.
 *
 * This page is requested rather than the user/ page because it makes sure that
 * a user is NOT authenticated when initiating the OpenID authentication
 * request.
 */
function openid_sso_relying_init_page() {
  openid_sso_relying_logout();
  drupal_goto('user');
}

/**
 * Page callback for syncing this site to with the provider.
 * Logs out user then runs back through the authentication process.
 *
 */
function openid_sso_relying_sync() {

  if (user_is_logged_in()) {

    global $user;

    watchdog('user', 'Session closed for %name.', array('%name' => $user->name));

    watchdog('openid_sso_relying', 'OpenID synchronization for %name in progress.', array('%name' => $user->name));

    module_invoke_all('user_logout', $user);

    // Destroy the current session, and reset $user to the anonymous user.
    session_destroy();

  }

  // Now begin the login process.
  $provider = variable_get('openid_sso_relying_provider', array());

    //  Check if a destination was passed in the URL.  If so, make sure the URL is clean.
  if (isset($_GET['destination'])) {
    $clean_destination = check_plain($_GET['destination']);
  } else {
    // If no destination was passed, set a default to the user page.
    $clean_destination = 'user';
  }

  // Add a variable to the destination so that we know to set a message for the user when done.
  $clean_destination .= '?openid_sso_sync_updated=1';

  // Override the destination if this is a REMOTE synchronization.
  if ($_GET['q'] == 'sso/synchronize/external/provider') {  // Change the destination if this is a remote synchronization.
    $clean_destination = 'sso/synchronize/external/provider/redirect?realm=' . check_plain($_GET['openid_sso_realm']);
  }

  // If in a colorbox, add another variable to the destination string.
  if ($_GET['q'] == 'sso/synchronize/link/colorbox' || isset($_GET['openid_sso_sync_colorbox'])) {
    $clean_destination .= '&openid_sso_sync_colorbox=1';
  }

          //  If this is in a COLORBOX from the RELYING PARTY site, we need to pass a few extra variables.
          if (isset($_GET['openid_profile_type'])) {
             $clean_destination .= '&openid_profile_type=' . check_plain($_GET['openid_profile_type']);

              // Check if a custom theme was set.
              if (isset($_GET['openid_profile_theme'])) {
                 $clean_destination .= '&openid_profile_theme=' . check_plain($_GET['openid_profile_theme']);

                    // Check if a custom BLANK theme was set.
                    if (isset($_GET['openid_profile_blank_theme'])) {
                       $clean_destination .= '&openid_profile_blank_theme=' . check_plain($_GET['openid_profile_blank_theme']);
                    }

                }

           }

  // Set the appropriate values.
   $values = array(
     'openid_identifier' => $provider['url'],
     'openid.return_to' => url('openid/authenticate', array('absolute' => TRUE, 'https' => TRUE, 'query' => array('destination' =>  $clean_destination))),
    );

   // Start the OpenID login process and redirect the user when done.
    openid_begin($values['openid_identifier'], $values['openid.return_to'], $values);

}

/**
 * Custom menu callback for user/page.
 */
function openid_sso_relying_user_page() {
  global $user;
  if ($user->uid) {
  module_load_include('pages.inc', 'user', 'user');
  return user_page();
  }
  return openid_sso_relying_request();
}

/**
 * Custom menu callback for user/register.
 */
function openid_sso_relying_user_register_page() {
  $provider = variable_get('openid_sso_relying_provider', array());

  $front = drupal_get_normal_path(variable_get('site_frontpage', 'node'));

    //  Check if a destination was passed in the URL.  If so, make sure the URL is clean.
  if (isset($_GET['destination'])) {
    $clean_destination = check_plain($_GET['destination']);
  } else {
    // If no destination was passed, set a default to the frontpage.
    $clean_destination = $front;
  }

  if ($_GET['q'] != 'sso/register' && (variable_get('openid_sso_relying_register_method', "default_link") == 'inline_iframe' ||
      variable_get('openid_sso_relying_register_method', "default_link") == 'colorbox_inline_iframe')) {
    // Embed the provider registration form into the page using an iframe.  Use the settings from the admin.


     // Check if the custom iframe OVERRIDE theme has been set, if so add it to the URL.
     // Otherwise, just let the OpenID SSO Provider use its default theme.
     if (variable_get('openid_sso_relying_iframe_theme_name') != NULL) {

          $iframe_url = url($provider['url'] . 'user/register/iframe', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'openid_sso_iframe' => TRUE,
                      'openid_sso_theme' => variable_get('openid_sso_relying_iframe_theme_name'),
                      'realm' => $GLOBALS['base_url'],
                      'destination' => $clean_destination)));

     } else {  // Pass the same URL minus the custom theme variable.

          $iframe_url = url($provider['url'] . 'user/register/iframe', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'openid_sso_iframe' => TRUE,
                      'realm' => $GLOBALS['base_url'],
                      'destination' => $clean_destination)));

     }

    $output = '<div class="openid-sso-relying-register"><h1>' . t($provider['name'] . ' user registration') . '</h1><br>' .
     '<iframe src="' . $iframe_url . '" name="provider_register" width="'
        . variable_get('openid_sso_relying_register_iframe_width', 500) . '"
       height="' . variable_get('openid_sso_relying_register_iframe_height', 500) . '" frameborder="0" overflow-x: hidden; overflow-y: scroll noresize="noresize">
      <p>Sorry, your browser does not support iframes. <br>
      ' . l(t('To register with the ' . $provider['name'] . ' please use this link.'), $provider['url'] . 'user/register', array('attributes' => array('target' => '_blank')))
          . '</p>

      </iframe>';
    return $output;


  } elseif ($_GET['q'] == 'sso/register' && (variable_get('openid_sso_relying_register_method', "default_link") == 'colorbox_redirect' ||
      variable_get('openid_sso_relying_register_method', "default_link") == 'colorbox_inline_iframe')) {
    // This IS a colorbox popup and the admin has set this as an option.
    // Redirect the page using the correct parameters.

    $_GET['destination'] = NULL;  // Remove the destination parameter to make sure the drupal_goto functions correctly.

     // Check if the custom colorbox OVERRIDE theme has been set, if so add it to the URL.
     // Otherwise, just let the OpenID SSO Provider use its default theme.
     if (variable_get('openid_sso_relying_colorbox_theme_name') != NULL) {

          $iframe_url = url($provider['url'] . 'user/register/iframe', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'openid_sso_colorbox' => TRUE,
                      'openid_sso_theme' => variable_get('openid_sso_relying_colorbox_theme_name'),
                      'realm' => $GLOBALS['base_url'],
                      'destination' => $clean_destination)));

     } else {  // Pass the same URL minus the custom theme variable.

          $iframe_url = url($provider['url'] . 'user/register/iframe', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'openid_sso_colorbox' => TRUE,
                      'realm' => $GLOBALS['base_url'],
                      'destination' => $clean_destination)));

     }

    drupal_goto($iframe_url);

   } else {

    // Default setup if nothing else matches.  This is just a redirect.
    $_GET['destination'] = NULL;  // Remove the destination parameter to make sure the drupal_goto functions correctly.

    drupal_goto(url($provider['url'] . 'user/register', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'destination' => $GLOBALS['base_url']))));
  }

}

/**
 * Custom menu callback for user/password.
 */
function openid_sso_relying_user_password_page() {
  $provider = variable_get('openid_sso_relying_provider', array());

  if ($_GET['q'] != 'sso/password' && (variable_get('openid_sso_relying_password_method', "default_link") == 'inline_iframe' ||
      variable_get('openid_sso_relying_password_method', "default_link") == 'colorbox_inline_iframe')) {
    // Embed the provider registration form into the page using an iframe.  Use the settings from the admin.

     // Check if the custom iframe OVERRIDE theme has been set, if so add it to the URL.
     // Otherwise, just let the OpenID SSO Provider use its default theme.
     if (variable_get('openid_sso_relying_iframe_theme_name') != NULL) {

          $iframe_url = url($provider['url'] . 'user/password', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'openid_sso_iframe' => TRUE,
                      'openid_sso_theme' => variable_get('openid_sso_relying_iframe_theme_name'),
                      'realm' => $GLOBALS['base_url'],
                      'destination' => 'empty_page?openid_sso_iframe=1&openid_sso_theme=' . variable_get('openid_sso_relying_iframe_theme_name'))));

     } else {  // Pass the same URL minus the custom theme variable.

          $iframe_url = url($provider['url'] . 'user/password', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'openid_sso_iframe' => TRUE,
                      'realm' => $GLOBALS['base_url'],
                      'destination' => 'empty_page?openid_sso_iframe=1')));

     }

    $output = '<div class="openid-sso-relying-password"><h2>' . t($provider['name'] . ' request new password') . '</h2><br>' .
     '<iframe src="' . $iframe_url . '" name="provider_password" width="' . variable_get('openid_sso_relying_password_iframe_width', 500) . '"
       height="' . variable_get('openid_sso_relying_password_iframe_height', 500) . '" frameborder="0" overflow-x: hidden; overflow-y: scroll noresize="noresize">
      <p>Sorry, your browser does not support iframes. <br>
      ' . l(t('To request a new password with the ' . $provider['name'] . ' please use this link.'), $provider['url'] . 'user/password', array('attributes' => array('target' => '_blank')))
          . '</p>
      </iframe>';
    return $output;

                // This IS a colorbox and the admin has selected a colorbox popup.
  } elseif ($_GET['q'] == 'sso/password' && (variable_get('openid_sso_relying_password_method', "default_link") == 'colorbox_redirect' ||
      variable_get('openid_sso_relying_password_method', "default_link") == 'colorbox_inline_iframe')) {

    // This IS a colorbox popup and the admin has set this as an option.
    // Redirect the page using the correct parameters.

    $_GET['destination'] = NULL;  // Remove the destination parameter to make sure the drupal_goto functions correctly.

     // Check if the custom colorbox OVERRIDE theme has been set, if so add it to the URL.
     // Otherwise, just let the OpenID SSO Provider use its default theme.
     if (variable_get('openid_sso_relying_colorbox_theme_name') != NULL) {

          $iframe_url = url($provider['url'] . 'user/password', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'openid_sso_colorbox' => TRUE,
                      'openid_sso_theme' => variable_get('openid_sso_relying_colorbox_theme_name'),
                      'realm' => $GLOBALS['base_url'],
                      'destination' => 'empty_page?openid_sso_colorbox=1&openid_sso_theme=' . variable_get('openid_sso_relying_colorbox_theme_name'))));

     } else {  // Pass the same URL minus the custom theme variable.

          $iframe_url = url($provider['url'] . 'user/password', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'openid_sso_colorbox' => TRUE,
                      'realm' => $GLOBALS['base_url'],
                      'destination' => 'empty_page?openid_sso_colorbox=1')));

     }

     drupal_goto($iframe_url);

  } else {
    // Default setup if nothing else matches.  This is just a redirect.
    $_GET['destination'] = NULL;  // Remove the destination parameter to make sure the drupal_goto functions correctly.

    drupal_goto(url($provider['url'] . 'user/password', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'destination' => $GLOBALS['base_url']))));
  }

}

/**
 * Custom logout callback, redirects to hub.
 */
function openid_sso_relying_logout_page() {

  global $user;
  // First check to see if ONLY local logouts are allowed.  If so, log the user out and don't go any further.
  if (variable_get('openid_sso_relying_logout_method', "default_link") == 'local_logout_only') {
    // User core logout process.
    module_load_include('pages.inc', 'user', 'user');
    user_logout();
  }

// Only redirect external users to provider page
  if (db_select('authmap', 'a')->fields('a', array('uid'))->condition('uid', $user->uid)->execute()->fetchField()) {
//    openid_sso_relying_logout(); // This might be better to be executed after provider logout via sso/logout-finalize
    $provider = variable_get('openid_sso_relying_provider', array());

    // Check to see if this is in a colorbox or not.
    if ($_GET['q'] == 'sso/logout' && variable_get('openid_sso_relying_logout_method', "default_link") == 'colorbox_redirect') {

    // This IS a colorbox popup and the admin has set this as an option.
    // Redirect the page using the correct parameters.

    $_GET['destination'] = NULL;  // Remove the destination parameter to make sure the drupal_goto functions correctly.

     // Check if the custom colorbox OVERRIDE theme has been set, if so add it to the URL.
     // Otherwise, just let the OpenID SSO Provider use its default theme.
     if (variable_get('openid_sso_relying_colorbox_theme_name') != NULL) {

          $iframe_url = url($provider['url'] . 'sso/logout', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'openid_sso_colorbox' => TRUE,
                      'openid_sso_theme' => variable_get('openid_sso_relying_colorbox_theme_name'),
                      'realm' => $GLOBALS['base_url'],
                      'logout_redirect' => url('sso/logout-finalize', array('absolute' => TRUE, 'https' => TRUE, 'query' => array('openid_sso_colorbox' => TRUE))),
                      )));

     } else {  // Pass the same URL minus the custom theme variable.

          $iframe_url = url($provider['url'] . 'sso/logout', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'openid_sso_colorbox' => TRUE,
                      'realm' => $GLOBALS['base_url'],
                      'logout_redirect' => url('sso/logout-finalize', array('absolute' => TRUE, 'https' => TRUE, 'query' => array('openid_sso_colorbox' => TRUE))),
                      )));

     }

     drupal_goto($iframe_url);

    } else {
      $_GET['destination'] = NULL;  // Remove the destination parameter to make sure the drupal_goto functions correctly.
      drupal_goto($provider['url'] . 'sso/logout/', array('query' => array('realm' => url(NULL, array('absolute' => TRUE,)),
                                                                         'logout_redirect' => url('sso/logout-finalize', array('absolute' => TRUE)))));

    }
  }
  else {
    // User core logout process.
    module_load_include('pages.inc', 'user', 'user');
    user_logout();
  }

}

/**
 * Request authentication.
 */
function openid_sso_relying_request() {
  module_load_include('pages.inc', 'user', 'user');

  $provider = variable_get('openid_sso_relying_provider', array());
  $front = drupal_get_normal_path(variable_get('site_frontpage', 'node'));

  //  Check if a destination was passed in the URL.  If so, make sure the URL is clean.
  if (isset($_GET['destination'])) {
    $clean_destination = check_plain($_GET['destination']);
  } else {
    // If no destination was passed, set a default to the frontpage.
    $clean_destination = $front;
  }

  $values = array(
    'openid_identifier' => $provider['url'],
    'openid.return_to' => url('openid/authenticate', array('absolute' => TRUE, 'https' => TRUE, 'query' => array('destination' => $clean_destination))),
  );

  // If the admin has selected for one of the embedded iframes, this is the output needed.
  if (variable_get('openid_sso_relying_login_method', "default_link") == 'inline_iframe' ||
      variable_get('openid_sso_relying_login_method', "default_link") == 'colorbox_inline_iframe') {
    // Embed the provider login form into the page using an iframe.  Use the settings from the admin.

    $iframe_url = url('sso/iframe/login', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                      'openid_sso_iframe' => TRUE,
                      'destination' => $clean_destination,
                      'realm' => $GLOBALS['base_url'],
                       )));

    $output = '<div class="openid-sso-relying-login"><h2>' . t($provider['name'] . ' user log in') . '</h2><br>' .
     '<iframe src="' . $iframe_url .
        '" name="provider_register" width="' . variable_get('openid_sso_relying_login_iframe_width', 500) . '"
       height="' . variable_get('openid_sso_relying_login_iframe_height', 500) . '" frameborder="0" overflow-x: hidden; overflow-y: scroll noresize="noresize">
      <p>Sorry, your browser does not support iframes. <br>'
      . l(t('To log in with the ' . $provider['name'] . ' please use this link.'),
          url('sso/iframe/login', array('absolute' => TRUE, 'https' => TRUE, 'query' => array('destination' => $clean_destination))),
          array('attributes' => array('target' => '_top'))) .
        '</p> </iframe>';
    return $output;


  } else {
    // Default setup if nothing else matches.  This is just a redirect.
    openid_begin($values['openid_identifier'], $values['openid.return_to'], $values);
  }

}

/**
 * Pillaged from user_logout().
 */
function openid_sso_relying_logout() {

  // Check if the logout was cancelled by the user.  If not, log the user out. If cancelled, skip ahead and redirect.
  // ALSO make sure the user is actually LOGGED IN before trying to logout.
  if (!isset($_GET['logout_cancelled']) && user_is_logged_in()) {

    global $user;

    watchdog('user', 'Session closed for %name.', array('%name' => $user->name));

    module_invoke_all('user_logout', $user);

    // Destroy the current session, and reset $user to the anonymous user.
    session_destroy();

  }

  //  If the colorbox attribute was passed in the url, remove ALL output so that it is a CLEAN page.
  //  This is done since we KNOW that the user MUST have javascript enabled and we want to keep the page
  //  clear during the redirect.  (It looks better that way.)

    if (isset($_GET['openid_sso_colorbox'])){
      //  In case we are in a colorbox, we will add a bit of javascript to break frames.
      $front = drupal_get_normal_path(variable_get('site_frontpage', 'node'));
      // Add inline javascript to redirect the user to the homepage.
      drupal_add_js('top.location.replace("' . url($front, array('absolute' => TRUE)) . '");', 'inline');

      $output = ' ';

      return $output;

    } else {
      // NOT in a colorbox so just redirect
      drupal_goto();

    }
 }

  /**
   * This page is being viewed in an embedded iframe.
   * Simply sent the user through the normal OpenID login process.  (While in an iframe.)
 */
function openid_sso_relying_iframe_login() {

  $provider = variable_get('openid_sso_relying_provider', array());
  $front = drupal_get_normal_path(variable_get('site_frontpage', 'node'));

    //  Check if a destination was passed in the URL.  If so, make sure the URL is clean.
  if (isset($_GET['destination'])) {
    $clean_destination = check_plain($_GET['destination']);
  } else {
    // If no destination was passed, set a default to the frontpage.
    $clean_destination = $front;
  }

  //  Set a SESSION variable to contain the current destination.
    $_SESSION['openid_sso_relying_destination'] = $clean_destination;

    // Redirect to the custom login area.
    // This is done in order to bust out of the iframe in a way that is the lease visible to the user.
    $destination = 'sso/iframe/login/redirect';

      if ($_GET['q'] == 'sso/login') {
        // This is in a colorbox, so add that to the return destination for additional themeing.
        $destination .= '?openid_sso_colorbox=true';

         // Check if the custom colorbox OVERRIDE theme has been set, if so add it to the URL.
         // Otherwise, just let the OpenID SSO Provider use its default theme.
         if (variable_get('openid_sso_relying_colorbox_theme_name') != NULL) {

            $values = array(
            'openid_identifier' => $provider['url'],
            'openid.return_to' => url('openid/authenticate', array('absolute' => TRUE, 'https' => TRUE, 'query' => array('destination' =>  $destination, 'openid_sso_colorbox' => TRUE, 'openid_sso_theme' => variable_get('openid_sso_relying_colorbox_theme_name')))),
              );

         } else {  // Pass the same URL minus the custom theme variable.

            $values = array(
            'openid_identifier' => $provider['url'],
            'openid.return_to' => url('openid/authenticate', array('absolute' => TRUE, 'https' => TRUE, 'query' => array('destination' =>  $destination, 'openid_sso_colorbox' => TRUE))),
              );

         }

      } else {
          //  This is NOT a colorbox popup.
            $destination .= '?openid_sso_iframe=true';

         // Check if the custom iframe OVERRIDE theme has been set, if so add it to the URL.
         // Otherwise, just let the OpenID SSO Provider use its default theme.
         if (variable_get('openid_sso_relying_iframe_theme_name') != NULL) {

            $values = array(
            'openid_identifier' => $provider['url'],
            'openid.return_to' => url('openid/authenticate', array('absolute' => TRUE, 'https' => TRUE, 'query' => array('destination' =>  $destination, 'openid_sso_iframe' => TRUE, 'openid_sso_theme' => variable_get('openid_sso_relying_iframe_theme_name')))),
              );

         } else {  // Pass the same URL minus the custom theme variable.

            $values = array(
            'openid_identifier' => $provider['url'],
            'openid.return_to' => url('openid/authenticate', array('absolute' => TRUE, 'https' => TRUE, 'query' => array('destination' =>  $destination, 'openid_sso_iframe' => TRUE))),
              );

         }

      }



    openid_begin($values['openid_identifier'], $values['openid.return_to'], $values);

}

  /**
   * This page is seen if the user has their javascript turned off, otherwise the user is automatically redirected.
   * It is necessary to have this page in order to redirect the user here after a successful login when it is done
   * through an embedded iframe or colorbox iframe.
 */
function openid_sso_relying_iframe_login_redirect() {
    global $base_url;

  //  Create a simple link to the passed destination link in case the user does not have javascript turned on.
  if (isset($_SESSION['openid_sso_relying_destination'])) {

    $clean_destination = check_plain($_SESSION['openid_sso_relying_destination']);

    unset($_SESSION['openid_sso_relying_destination']); // Get rid of the variable now that it is not needed.

    // Add inline javascript to redirect the user to the passed "destination".
    drupal_add_js('top.location.replace("' . url($clean_destination, array('absolute' => TRUE, 'https' => TRUE)) .'");', 'inline');
    drupal_set_message('You have Successfully Logged In ' . l(t('Click here to continue.'), url($clean_destination, array('absolute' => TRUE, 'https' => TRUE)), array('attributes' => array('target' => '_top'))));
  } else {
    $front = drupal_get_normal_path(variable_get('site_frontpage', 'node'));

    // Add inline javascript to redirect the user to the homepage.
    drupal_add_js('top.location.replace("' . $front .'");', 'inline');

    // Otherwise, just redirect the user to the homepage.
    drupal_set_message('You have Successfully Logged In ' . l(t('Click here to continue.'), url($front, array('absolute' => TRUE, 'https' => TRUE)), array('attributes' => array('target' => '_top'))));

  }


    // Set a SESSION variable in order to set a message after the redirect.
    $_SESSION['openid_sso_relying_login_successful'] = TRUE;

  ////  If the colorbox attribute was passed in the url, remove ALL output so that it is a CLEAN page.
  //  This is done since we KNOW that the user MUST have javascript enabled and we want to keep the page
  //  clear during the redirect.  (It looks better that way.)
//  if (isset($_GET['openid_sso_colorbox'])){
//     $output = ' ';
//     drupal_set_title(' ');
//    }
    $output = ' ';

    return $output;

}

/*
 * A simple redirect to send the user to their OpenID SSO Identity Provider account.
 */

function openid_sso_relying_network_profile_redirect() {
    $provider = variable_get('openid_sso_relying_provider', array());

    $_GET['destination'] = NULL;  // Remove the destination parameter to make sure the drupal_goto functions correctly.

    drupal_goto($provider['url'] . 'user', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                  'destination' => $GLOBALS['base_url'] . '/user',
            )));  // Redirect to CURRENT user's edit page.

}

/*
 * A simple redirect to send the user to their OpenID SSO Identity Provider account.
 */

function openid_sso_relying_password_redirect() {

    $_GET['destination'] = NULL;  // Remove the destination parameter to make sure the drupal_goto functions correctly.

    drupal_goto('user/password', array('absolute' => TRUE, 'https' => TRUE));  // Redirect to the password request page.

}

/*
 * A simple redirect to send the user to their account after they have been synchronized.
 */

function openid_sso_relying_sync_colorbox_redirect() {

    $_GET['destination'] = NULL;  // Remove the destination parameter to make sure the drupal_goto functions correctly.

    $provider = variable_get('openid_sso_relying_provider', array());

    drupal_set_message(t('Your account has been successfully synchronized with your ' . $provider['name'] . ' profile.'));  // Notify user.
    drupal_goto('user');  // Redirect user to their account.

}

/*
 * This function does a few checks and begins the synchronization if everything is correct.
 */

function openid_sso_relying_remote_sync() {
if (variable_get('openid_sso_relying_remote_sync') == 1) {  // ONLY go further IF this feature is ACTIVE.

    //  Get the provider info in order to compare.
    $provider = variable_get('openid_sso_relying_provider', array());

    if (!isset($_GET['openid_sso_realm']) || $_GET['openid_sso_realm'] != trim($provider['url'], '/')) {
      watchdog('openid_sso_relying', 'OpenID EXTERNAL synchronization failed. Either the wrong "provider realm" was
        sent, or none at all.', array(), WATCHDOG_WARNING);  // Keep a record of the occurence.

      drupal_access_denied();   // Deny any further access since this is NOT a valid request.

      return FALSE;
    }

    if (!isset($_GET['openid_sso_api']) || $_GET['openid_sso_api'] != variable_get('openid_sso_relying_remote_sync_api')) {
      watchdog('openid_sso_relying', 'OpenID EXTERNAL synchronization failed. Either no API Key was
        sent or the wrong one was used.', array(), WATCHDOG_WARNING);  // Keep a record of the occurence.

      drupal_access_denied();   // Deny any further access since this is NOT a valid request.

      return FALSE;
    }

    if (user_is_logged_in()) { // If the user is currently logged in, check possible MINIMUM timing settings.

       global $user;

       if (variable_get('openid_sso_relying_remote_sync_enforce_minimum_time') == 1 && // MINIMUM time settings are enforced.)
          ($user->login + intval(variable_get('openid_sso_relying_sync_minimum_time_limit', 0))) < time()) {  // Not enough time has passed.
              drupal_access_denied();   // Deny any further access since there has NOT been enough time.

              return FALSE;
       }

    watchdog('openid_sso_relying', 'REMOTE Synchronization has begun for ' . $user->name);  // Keep a record of the occurence.

    }

  // Begin the synchronization process.
  openid_sso_relying_sync();

} else {
      drupal_access_denied();   // Deny any further access since this is NOT a valid request.

      return FALSE;
}
}

/*
 * A simple redirect to send the user back to the provider site after they have been REMOTELY synchronized.
 */

function openid_sso_relying_remote_sync_redirect() {

    $_GET['destination'] = NULL;  // Remove the destination parameter to make sure the drupal_goto functions correctly.

    global $base_url;
    global $user;
    $provider = variable_get('openid_sso_relying_provider', array());

    watchdog('openid_sso_relying', 'REMOTE Synchronization has finished for ' . $user->name);  // Keep a record of the occurence.

          // Set the basic query parameters that will be needed.
          $query_parameters = array('realm' => $base_url,
                              'site_name' => variable_get('site_name'),
            );

          // Now start doing a few checks and adding additional parameters when needed.

          if (isset($_GET['openid_sso_sync_colorbox'])) {
             $query_parameters['openid_sso_sync_colorbox'] = TRUE;

          }

          //  If this is in a COLORBOX from the RELYING PARTY site, we need to pass a few extra variables.
          if (isset($_GET['openid_profile_type'])) {
             $query_parameters['openid_profile_type'] = check_plain($_GET['openid_profile_type']);

              // Check if a custom theme was set.
              if (isset($_GET['openid_profile_theme'])) {
                 $query_parameters['openid_profile_theme'] = check_plain($_GET['openid_profile_theme']);

                    // Check if a custom BLANK theme was set.
                    if (isset($_GET['openid_profile_blank_theme'])) {
                       $query_parameters['openid_profile_blank_theme'] = check_plain($_GET['openid_profile_blank_theme']);
                    }

                }

           }


    drupal_goto($provider['url'] . 'sso/network-sites/sync/redirect', array('absolute' => TRUE, 'https' => TRUE,
                'query' => array($query_parameters)));  // Redirect user to their account.

}

