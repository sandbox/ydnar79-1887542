(function ($) {
  Drupal.behaviors.openid_sso_relying_in_network_tab_colorbox = {
    attach: function(context, settings) {
      if (!$.isFunction($.colorbox)) {  // Check if colorbox is available.
        return;
      }

          var uid = Drupal.settings.openid_sso_relying_logged_in_network.uid;

        // Attach colorbox to edit links. This falls back gracefully without JS.
      $("a[href*='/user/" + uid + "/network_profile'], a[href*='?q=user/" + uid + "/network_profile']", context).each(function() {
          var provider_path = Drupal.settings.openid_sso_relying_logged_in_network.provider_url;
          var openid_profile_theme = Drupal.settings.openid_sso_relying_logged_in_network.openid_profile_theme;
          var openid_profile_blank_theme = Drupal.settings.openid_sso_relying_logged_in_network.openid_profile_blank_theme;
          var openid_profile_type = Drupal.settings.openid_sso_relying_logged_in_network.openid_profile_type;

          // Check which variables were passed so we know what to send to the Identity Provider.
          // This is a FULL profile.
          if (openid_profile_type == 'full_profile') {
            var link = '?openid_profile_type=full_profile';
          } // This is an EDIT PROFILE ONLY link.
          else {
            var link = '?openid_profile_type=edit_only';
          }


          // Check to see if the theme name was passed and if so add it to the link.
          if (typeof(openid_profile_theme) != "undefined" && openid_profile_theme !== null && openid_profile_theme !== '') {
              var link = link + '&openid_profile_theme=' + openid_profile_theme;

              // If the full theme was passed, check if the BLANK theme is also passed.
              if (typeof(openid_profile_blank_theme) != "undefined" && openid_profile_blank_theme !== null && openid_profile_blank_theme !== '') {
                  var link = link + '&openid_profile_blank_theme=' + openid_profile_blank_theme;
              }
          }

            this.href = provider_path + link + '&destination=user' + link;

      $(this).colorbox({
          speed: 500,
          iframe: true,
          width: '90%',
          height: '90%',

          onClosed: function() {
            // Force a page refresh after colorbox has been closed.
            window.location.reload(false);
          }
        });
      });
    }
  };
})(jQuery);

