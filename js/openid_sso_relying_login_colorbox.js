(function ($) {
  Drupal.behaviors.openid_sso_relying_login_colorbox = {
    attach: function(context, settings) {
      if (!$.isFunction($.colorbox)) {  // Check if colorbox is available.
        return;
      }

        // Attach colorbox to edit links. This falls back gracefully without JS.
      $("a[href*='/user/login'], a[href*='?q=user/login'], a[href*='/user'], a[href*='?q=login']", context).each(function() {
          var path = this.href;
          var new_path = path.replace('user','sso/login'); // Replace the standard path.
          var new_path = new_path.replace(/sso\/login\/login/,'sso/login');  // Fix login links that may have gotten broken.
          var new_path = new_path.replace(/sso\/login\/password/,'user/password');  // Fix password links that may have gotten broken.
          var new_path = new_path.replace(/sso\/login\/register/,'user/register');  // Fix registration links that may have gotten broken.
          var new_path = new_path.replace(/user\/login/,'sso/login');  // One final check.
          var addquery = (path.indexOf('?') !=-1) ? '&' : '?';
          var dest = window.location.pathname.replace(Drupal.settings.basePath, '');

          // If no destination, add one to the current page.
          if (path.indexOf('destination') != -1 || dest == '') {
            this.href = new_path;
          }
          else {
            this.href = new_path + addquery + 'destination=' + dest;
          }

      $(this).colorbox({
          speed: 500,
          iframe: true,
          width: Drupal.settings.openid_sso_relying_login.colorbox_width,
          height: Drupal.settings.openid_sso_relying_login.colorbox_height,
          title: Drupal.settings.openid_sso_relying_login.colorbox_title,

          onClosed: function() {
            // Force a page refresh after colorbox has been closed.
            window.location.reload(false);
          }
        });
      });
    }
  };
})(jQuery);
