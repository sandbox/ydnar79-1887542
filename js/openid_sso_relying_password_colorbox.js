(function ($) {
  Drupal.behaviors.openid_sso_relying_password_colorbox = {
    attach: function(context, settings) {
      if (!$.isFunction($.colorbox)) {  // Check if colorbox is available.
        return;
      }

        // Attach colorbox to edit links. This falls back gracefully without JS.
      $("a[href*='/user/password'], a[href*='?q=user/password']", context).each(function() {
          var path = this.href;
          var new_path = path.replace(/user\/password/,'sso/password');
          var addquery = (path.indexOf('?') !=-1) ? '&' : '?';
          var dest = window.location.pathname.replace(Drupal.settings.basePath, '');

          // If no destination, add one to the current page.
          if (path.indexOf('destination') != -1 || dest == '') {
            this.href = new_path;
          }
          else {
            this.href = new_path + addquery + 'destination=' + dest;
          }

      $(this).colorbox({
          speed: 500,
          iframe: true,
          width: Drupal.settings.openid_sso_relying_password.colorbox_width,
          height: Drupal.settings.openid_sso_relying_password.colorbox_height,
          title: Drupal.settings.openid_sso_relying_password.colorbox_title,

          onClosed: function() {
            // Force a page refresh after colorbox has been closed.
            window.location.reload(false);
          }
        });
      });
    }
  };
})(jQuery);
