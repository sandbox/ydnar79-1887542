(function ($) {
  Drupal.behaviors.openid_sso_relying_logged_in_password_colorbox = {
    attach: function(context, settings) {
      if (!$.isFunction($.colorbox)) {  // Check if colorbox is available.
        return;
      }

          var uid = Drupal.settings.openid_sso_relying_logged_in_password.uid;

        // Attach colorbox to edit links. This falls back gracefully without JS.
      $("a[href*='/user/" + uid + "/password'], a[href*='?q=user/" + uid + "/password']", context).each(function() {
          var path = this.href;
          var base_url = Drupal.settings.openid_sso_relying_logged_in_password.base_url;
          var new_path = base_url + '/sso/password';
          var addquery = (path.indexOf('?') !=-1) ? '&' : '?';
          var dest = window.location.pathname.replace(Drupal.settings.basePath, '');

          // If no destination, add one to the current page.
          if (path.indexOf('destination') != -1 || dest == '') {
            this.href = new_path;
          }
          else {
            this.href = new_path + addquery + 'destination=' + dest;
          }

      $(this).colorbox({
          speed: 500,
          iframe: true,
          width: Drupal.settings.openid_sso_relying_logged_in_password.colorbox_width,
          height: Drupal.settings.openid_sso_relying_logged_in_password.colorbox_height,
          title: Drupal.settings.openid_sso_relying_logged_in_password.colorbox_title,

          onClosed: function() {
            // Force a page refresh after colorbox has been closed.
            window.location.reload(false);
          }
        });
      });
    }
  };
})(jQuery);
