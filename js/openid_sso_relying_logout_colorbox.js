(function ($) {
  Drupal.behaviors.openid_sso_relying_logout_colorbox = {
    attach: function(context, settings) {
      if (!$.isFunction($.colorbox)) {  // Check if colorbox is available.
        return;
      }

        // Attach colorbox to edit links. This falls back gracefully without JS.
      $("a[href*='/user/logout'], a[href*='?q=user/logout']", context).each(function() {
          var path = this.href;
          var new_path = path.replace(/user\/logout/,'sso/logout');

            this.href = new_path;

      $(this).colorbox({
          speed: 500,
          iframe: true,
          width: Drupal.settings.openid_sso_relying_logout.colorbox_width,
          height: Drupal.settings.openid_sso_relying_logout.colorbox_height,
          title: Drupal.settings.openid_sso_relying_logout.colorbox_title,

          onClosed: function() {
            // Force a page refresh after colorbox has been closed.
            window.location.reload(false);
          }
        });
      });
    }
  };
})(jQuery);
