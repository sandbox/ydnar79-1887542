(function ($) {
  Drupal.behaviors.openid_sso_relying_synchronize_colorbox = {
    attach: function(context, settings) {
      if (!$.isFunction($.colorbox)) {  // Check if colorbox is available.
        return;
      }

        // Attach colorbox to links. This falls back gracefully without JS.
      $("#synchronize-button", context).click(function() {

      $(this).colorbox({
          href:Drupal.settings.openid_sso_relying_synchronize_button_url,
          speed: 0,
          iframe: true,
          width: '0px',
          height: '0px',
          overlayClose: false,
          escKey: false,
          opacity: 0


        });
      });
    }
  };
})(jQuery);
