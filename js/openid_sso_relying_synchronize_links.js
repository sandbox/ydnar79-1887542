(function ($) {
  Drupal.behaviors.openid_sso_relying_synchronize_links = {
    attach: function(context, settings) {

      /* Show a modal message when user clicks on "Login" (there may be a wait). */
      $("a[href*='/sso/synchronize/link'], a[href*='?q=/sso/synchronize/link']", context).click(function() {

          $.blockUI({ css: {border: 'none', background: 'transparent', color: '#CCCCCC'}, message:  Drupal.settings.openid_sso_relying_synchronize_link_wait_message});
      });
    }
  };
})(jQuery);
