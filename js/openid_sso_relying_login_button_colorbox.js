(function ($) {
/* Show a modal message when user clicks on "Login" (there may be a wait). */

Drupal.behaviors.openid_sso_relying_login_button_colorbox = {};
Drupal.behaviors.openid_sso_relying_login_button_colorbox.attach = function(context) {
  $('input.login-submit').click( function () {

      $(this).colorbox({
          href: Drupal.settings.openid_sso_relying_login.login_url,
          title: Drupal.settings.openid_sso_relying_login.colorbox_title,
          speed: 500,
          iframe: true,
          width: Drupal.settings.openid_sso_relying_login.colorbox_width,
          height: Drupal.settings.openid_sso_relying_login.colorbox_height,

          onClosed: function() {
            // Force a page refresh after colorbox has been closed.
            window.location.reload(false);
          }
        });

  });
}

})(jQuery);
