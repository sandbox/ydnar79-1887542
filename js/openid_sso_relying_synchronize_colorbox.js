(function ($) {
  Drupal.behaviors.openid_sso_relying_synchronize_colorbox = {
    attach: function(context, settings) {
      if (!$.isFunction($.colorbox)) {  // Check if colorbox is available.
        return;
      }

        // Attach colorbox to links. This falls back gracefully without JS.
      $("a[href*='sso/synchronize/link'], a[href*='?q=sso/synchronize/link']", context).each(function() {
          var path = this.href;
          var new_path = path.replace(/sso\/synchronize\/link/,'sso/synchronize/link/colorbox');

            this.href = new_path;

      $(this).colorbox({
          speed: 0,
          iframe: true,
          width: '0px',
          height: '0px',
          overlayClose: false,
          escKey: false,
          opacity: 0


        });
      });
    }
  };
})(jQuery);
