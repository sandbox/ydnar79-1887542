(function ($) {
  Drupal.behaviors.openid_sso_relying_synchronize_button = {
    attach: function(context, settings) {

      /* Show a modal message when user clicks on "Login" (there may be a wait). */
      $("#synchronize-button", context).click(function() {

          $.blockUI({ css: {border: 'none', background: 'transparent', color: '#CCCCCC'}, message:  Drupal.settings.openid_sso_relying_synchronize_button_wait_message});
      });
    }
  };
})(jQuery);
